<?php
/**
 * @package   local_calllearning
 * @copyright 2018, CALL Learning SAS
 * @author Laurent David <laurent@call-learning.fr>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

function xmldb_local_calllearning_upgrade($oldversion) {
    global $CFG, $DB;
    $dbman = $DB->get_manager(); // loads ddl manager and xmldb classes
    
    $success = true;
    if ($oldversion < 2018062204) {
        $success = calllearning_setups();
        upgrade_plugin_savepoint(true, 2018062204, 'local','calllearning');
    }
    
    
    return $success;
}
