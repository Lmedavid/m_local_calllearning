<?php
/**
 * @package   local_calllearning
 * @copyright 2018, CALL Learning SAS
 * @author Laurent David <laurent@call-learning.fr>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
$string['pluginname']   = 'CALL Learning Local Plugin';

// Settings string
$string['calllearningsettings'] = 'Paramètres CALL Learning';


// CRON
$string['createupdatecohorts'] = 'Fait en sorte que les cohortes et modules d\'inscriptions aux cours soient bien mis en place';