<?php
/**
 * @package   local_calllearning
 * @copyright 2018, CALL Learning SAS
 * @author Laurent David <laurent@call-learning.fr>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
$string['pluginname']   = 'CALL Learning Local Plugin';

// Settings string
$string['calllearningsettings'] = 'CALL Learning Settings';

// CRON
$string['createupdatecohorts'] = 'Make sure that the cohorts and right enrolments are created in the relevant courses';