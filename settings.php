<?php
/**
 * @package   local_calllearning
 * @copyright 2018, CALL Learning SAS
 * @author Laurent David <laurent@call-learning.fr>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**Setup Page*/

if ($hassiteconfig) {
    global $CFG;
    $calllearningsettings = new admin_settingpage('local_calllearning', get_string('calllearningsettings', 'local_calllearning'));
    $ADMIN->add('localplugins', $calllearningsettings);
}

