INTRODUCTION

This plugin encapsulates overrides for the CALL Learning Moodle Project

HOW TO SETUP


To be able to use this module you need first to install it using the usual Moodle admin install procedure (https://docs.moodle.org/28/en/Installing_plugins)
We use here the customsscript moodle setting (https://docs.moodle.org/dev/customscripts).
So to be able to override the view mechanisms of the quiz, you need to add the following line in your config.php file:

  $CFG->customscripts = dirname(__FILE__). '/local/calllearning/calllearning/';

INSTRUCTIONS

There is a specific category called 'ENTRY_COURSES' (name can be changed in the code), in which we have a series of single
entry courses that are the entry survey and entry tests for the platform.

Each course in this category will have a specific shortname (unique) that is the code for the cohort to be created/synchronised.

So for example if we have an entry test for TOIEC English groups, we will have a course with the unique ID TOIEC-EN, then a regular
cron script will create a cohort with the same name (shortname = TOIEC-EN, name = Course fullname => course name in the ENTRY_COURSES)
We will also enable the cohort enrolment in the matching course, so users of this cohort are enrolled via cohort sync.


CEFR and other Frameworks for Learning plans

In the future, the module *will* import basic framework for CEFR and other ECDL framework into competencies. The competencies
can then be assigned to a learning plan template.
Learning plan can then be attached to competencies which in turn can be attached to courses
(i.e. the courses are attached to the competencies in fact).
The dashboard will have to be modified to display a list of course with set competencies (instead of competencies listed and then courses).


TROUBLESHOOTING
git
tbd
