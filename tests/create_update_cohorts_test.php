<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Unit tests for the tag score class which build an array of scores
 * for each tag on a given course
 *
 * @package   local_enva
 * @category  phpunit
 * @copyright 2018, CALL Learning SAS
 * @author Laurent David <laurent@call-learning.fr>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


defined('MOODLE_INTERNAL') || die();

/**
 * This class contains the test cases for the tag_score class
 *
 */
class local_user_registration_testcase extends advanced_testcase {
    
    const COURSE_ROOT_ID = 'COURSE';
    
    public function test_create_cohort() {
        global $CFG;
        require_once($CFG->dirroot.'/local/calllearning/locallib.php');
        $this->resetAfterTest();
        list($category, $courses) = $this->create_courses_and_category();
        
        // Explicitely run the create_update_cohorts
        create_update_cohorts();
        
        // Now check that the cohort exists
        $allcohorts = cohort_get_all_cohorts()['cohorts'];
        $cohortcreated =  0;
        foreach($allcohorts as $c) {
            if (substr($c->idnumber,0, strlen(self::COURSE_ROOT_ID) ) == self::COURSE_ROOT_ID ) {
                $cohortcreated ++;
            }
        }
        $this->assertEquals(3, $cohortcreated, 'Three cohorts should have been created');
    }
    
    public function test_create_enrolment() {
        global $CFG, $DB;
        require_once($CFG->dirroot.'/local/calllearning/locallib.php');
        $this->resetAfterTest();
        $studentrole = $DB->get_record('role', array('shortname' => 'student'));
        list($category, $courses) = $this->create_courses_and_category();
        
        // Explicitely run the create_update_cohorts
        create_update_cohorts();
        // Call it twice to see if it does not create duplicate enrolments
        create_update_cohorts();
        
        // Now check that the enrolment exists and are activated
    
        foreach($courses as $c) {
            $einstances = enrol_get_instances($c->id,true);
            $cinstances = array_filter($einstances, function($cohort) {
                return ($cohort->enrol == 'cohort');
            });
            $this->assertTrue(count($cinstances) > 0, "There should be at least one enrolment for course {$c->idnumber}");
            $this->assertTrue(count($cinstances) == 1, "There should be only one enrolment for course {$c->idnumber}");
            foreach($cinstances as $ci) {
                $this->assertTrue($ci->roleid == $studentrole->id, "The role should be assigne to student role");
            }
        }
    }
    
    protected function create_courses_and_category($numcourses = 3) {
        // Create a course and a category
        $category = $this->getDataGenerator()->create_category(array('idnumber'=>ENTRY_COURSES_CAT_IDNUMBER));
        $courses =  array();
        for($ci = 1 ; $ci <= $numcourses ; $ci++) {
            array_push($courses ,
                $this->getDataGenerator()->create_course(array('category'=>$category->id, 'idnumber'=>self::COURSE_ROOT_ID.$ci)));
        }
        
        return array($category,$courses);
    }
}
