<?php

/**
* @package   local_calllearning
* @copyright 2018, CALL Learning SAS
* @author Laurent David <laurent@call-learning.fr>
* @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
*/

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2018062204;
$plugin->requires  =  2017051503.08;
$plugin->cron      = 0;
$plugin->component = 'local_calllearning';
$plugin->maturity  = MATURITY_STABLE;
$plugin->release   = 'v1.0';

$plugin->dependencies = array();