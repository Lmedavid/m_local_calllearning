<?php
/**
 * @package   local_calllearning
 * @copyright 2018, CALL Learning SAS
 * @author Laurent David <laurent@call-learning.fr>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;
global $CFG;

include_once($CFG->dirroot . '/local/calllearning/lib.php');

/**
 * Create or update the cohorts within the
 * @return bool
 * @throws coding_exception
 * @throws dml_exception
 */
function create_update_cohorts() {
    global $DB, $CFG;
    include_once($CFG->dirroot . '/cohort/lib.php');
    include_once($CFG->libdir . '/coursecatlib.php');
    $studentrole = $DB->get_record('role', array('shortname' => 'student'));
    
    $allcats = coursecat::get_all();
    $allcourses = array();
    foreach ($allcats as $cat) {
        if ($cat->idnumber == ENTRY_COURSES_CAT_IDNUMBER) {
            $allcourses = array_merge($allcourses, $cat->get_courses()); // TODO check if visibility can interfere with this, it should not if we are admin
        }
    }
    $allcohorts = cohort_get_all_cohorts()['cohorts'];
    $context = context_system::instance();
    
    $plugin = enrol_get_plugin('cohort');
    
    
    foreach ($allcourses as $c) {
        // Check if a cohort with the same course ID number exists
        $cohortexists = false;
        if (trim($c->idnumber) == '') {
            continue; // Nothing to do here
        }
        $existingcohort = null;
        foreach ($allcohorts as $cohort) {
            if ($c->idnumber == $cohort->idnumber) {
                $existingcohort = $cohort;
                break;
            }
        }
        if ($existingcohort) {
            // Update name
            $existingcohort->name = $c->fullname;
            $cohortid = $existingcohort->id;
            cohort_update_cohort($existingcohort);
        } else {
            $cohortid = cohort_add_cohort((object)array(
                'idnumber' => $c->idnumber,
                'name' => $c->fullname,
                'contextid' => $context->id
            ));
        }
        // Make sure that course enrolment is set to the right cohort
        $existing_enrolments = enrol_get_instances($c->id, true);
        $cinstances = array_filter($existing_enrolments, function ($cohort) use ($cohortid) {
            return ($cohort->enrol == 'cohort' && $cohort->customint1 == $cohortid);
        });
        $instancesettings = array('customint1' => $cohortid, 'roleid' => $studentrole->id,
            'customint2' => 0 // This is just to make sure we can call update_instance as it checks that this
            // value exist for data. See cohort/lib.php: 148
        );
        if (empty($cinstances)) {
            $plugin = enrol_get_plugin('cohort');
            $plugin->add_instance($c, array('customint1' => $cohortid, 'roleid' => $studentrole->id));
        } else {
            $instance = reset($existing_enrolments);
            $plugin->update_instance($instance, (object)$instancesettings);
        }
    }
    return true;
}

/**
 * Set the side global preferences
 * @return bool
 */
function setup_preferences() {
    set_config("defaulthomepage", HOMEPAGE_MY);
    set_config("enablecompletion", 1, "moodlecourse");
    set_config("enablecompletion", 1);
    return true;
}

/**
 * Setup default language to french
 *
 */
function setup_language() {
    global $CFG;
    include_once($CFG->dirroot . '/admin/tool/langimport/lib.php');
    tool_langimport_preupgrade_update('fr');
    set_config('lang', 'fr');
}

/**
 * Global setup function calling all setup function of this module
 * @return bool
 * @throws coding_exception
 * @throws dml_exception*
 */
function calllearning_setups() {
    return setup_language() && setup_preferences() && create_update_cohorts();
}